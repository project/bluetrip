
What is BlueTrip for Drupal ?
-----------------------------

The BlueTrip theme for Drupal is the adaptation of the CSS framework "BlueTrip"
(developed by Mike Crittenden <http://mikethecoder.com>) for the Drupal system.

You can find the original BlueTrip framework at http://bluetrip.org/

BlueTrip from bluetrip.org:

"A full featured and beautiful CSS (Cascading Style Sheets) framework which
combined the best of Blueprint, Tripoli (hence the name), Hartija's print
style-sheet, 960.gs's simplicity, and Elements' icons. BlueTrip gives you a
sensible set of styles and a common way to build a website so that you can
skip past the grunt work and get right to designing."

What is a Grid System ?
-----------------------

A grid system is a set of css class and id that you can use to quickly create
complicated layouts. When using a grid system, you can define the aspect of
the page by just dropping the appropriate class in your markup.

BlueTrip creates an invisible grid within your page, and provides you with a
24-column grid inside of a 950px wide container. You can use this grid to
set the width of element by just changing their class.

To set the width of a section, you'll be using div class="span-XX" where XX is
the number of columns (out of 24) you want that section to take up. For example,
if you want two equal sized columns, you would do div class="span-12" for the
first element, and div class="span-12 last" for the second element. Note that
the last element in the row should always have the "last" class so BlueTrip
knows not to add any padding to the right of that one.

You can also add empty columns before and after a section for white space. This
is done using the prefix-XX and suffix-XX classes. Obviously, prefix-XX adds XX
blank columns before the section, and suffix-XX adds XX columns after the
section. So say you want to have a 3-column layout with an empty column between
each of the main columns. That requires nothing more than 3 divs in a row, the
first with class="span-5 suffix-1", the second with class="span-12 suffix-1",
the last with class="span-5 last".

The advantages of the grid system are many :

* Flexibility: Want a 4-column layout? Or perhaps a 3-column with an extra
column of white space in between each? You got it.
* Browser Compatibility: Fully tested.
* Speed: Do anything you can do from scratch in half the time.

If you're still confused, check out the examples at http:bluetrip.org/demos


BlueTrip Feature List :
------------------------

* 24-column grid
* Sensible typography styles
* Clean form styles
* A print style-sheet
* An empty starter style-sheet
* Sexy buttons
* Status message styles
* Hover infos on span for admin (from 960)
* Show/Hide link for admin (built for Drupal)
* Build from the BASIC 2 theme for Drupal, and therefor include all Basic 2
features, like body classes, clean id/class system, etc...


What has been done to make it into a Drupal theme ?
---------------------------------------------------

The adaptation of BlueTrip to a Drupal theme have been done by using BASIC 2
as a starting point. Most the BASIC default css styles have been removed and
replaced by BlueTrip's css. The css styles from BlueTrip have been preserved
but also extended to the Drupal default output (like input or tabs).

Then the page.tpl.php has been adapted so it accepts one to three columns
layout by default, and so it is easy to modify.

There have been modifications only very few modification to the other templates
like node/block/box. And even template.php is practically the original from
BASIC 2, except that the regions have been commented by default.

I added a link for the admin user (user 1) to show/hide the grid. This is very
useful as the grid is only needed every once in a while to check the layout.
You can activate or deactivate the grid by just clicking on the link that should
be on the bottom left of the browser window.

I also added the hover effect, made in the debug.css, adapted from the 960
grid system. To do this, I had to create some original images, as the 960 grid
system only goes up to 16 columns.

In the Drupal 6 version and later, you can configure wether you want to have the
'edit' and 'configure' links on blocks when overing them, and you can also
enable the 'auto-cache-clearing' function, that will rebuilt the theme registry
on each page load. This is a nice add on when developing, but remember to turn
it of before launching the site, because it tends to use a lot of memory.

How to install ?
----------------

1. Download BlueTrip from http://drupal.org/project/bluetrip

2. Unpack the downloaded file and place the bluetrip folder in your Drupal
   installation under one of the following locations:

		sites/all/themes
    -> making it available to all Drupal sites in a multi-site configuration

		sites/default/themes
    -> making it available to only the default Drupal site

		sites/example.com/themes
    -> making it available to only the example.com site if there is a
       sites/example.com/settings.php configuration file

3. Log in as an administrator on your Drupal site and go to Administer > Site
   building > Themes (admin/build/themes) and make BlueTrip the default theme.

The modified BlueTrip framework is included, so no need to download it, thanks
to Mike Crittenden at at Capsize Designs.

Modifying the CSS styles
-------------------------

To modify the css styles, use the style.css file and not the screen.css file,
this is to preserve the integrity of the BlueTrip framework

Thanks
-------

Thanks to Mike Crittenden at Capsize Designs for developing the original
BlueTrip framework, and allowing me to contribute it to Drupal
http://bluetrip.org/
http://mikethecoder.com

Thanks to Raincity Studios for helping me during the development of this theme
http://www.raincitystudios.com/

Thanks to me :
http://www.couzinhub.com/en/home

And thanks to you for using BlueTrip for Drupal!

