<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language ?>" lang="<?php echo $language->language ?>" dir="<?php echo $language->dir ?>">
  <head>
    <title><?php echo $head_title; ?></title>
    <?php echo $head; ?>
    <?php echo $styles; ?>
    <!--[if IE]>
      <style type="text/css" media="all">
        @import "<?php echo $base_path . path_to_theme() ?>/css/ie.css";
      </style>
    <![endif]-->
    <?php echo $scripts; ?>
  </head>

  <body class="<?php echo $body_classes; ?>">
    <div id="skip-nav"><a href="#content">Skip to Content</a></div>  

    <div id="page" class="container">

    <!-- ______________________ HEADER _______________________ -->

      <?php // if($search_box) {echo $search_box;} ?>

      <?php if (!empty($logo)): ?>
        <div class="span-2 prefix-1">
          <a href="<?php echo $base_path; ?>" title="<?php echo t('Home'); ?>" rel="home" id="logo">
            <img src="<?php echo $logo; ?>" alt="<?php echo t('Home'); ?>" />
			  	</a>
				</div>
		  <?php endif; ?>
	      
      <?php if (!empty($site_name) || !empty($site_slogan)): ?>
			  
			  <?php if (!empty($logo) && $search_box): ?> <!-- Logo and search box -->
			    <div class="span-11">
        <?php elseif (!empty($logo)  && !$search_box): ?> <!-- Logo and NO search box -->
          <div class="span-21 last">
        <?php elseif (empty($logo)  && $search_box): ?> <!-- NO Logo and search box -->
          <div class="span-14">
        <?php elseif (empty($logo)  && !$search_box): ?> <!-- NO Logo and NO search box -->
          <div class="span-24 last">
        <?php endif; ?>
        
          <?php if (!empty($site_name)): ?>
            <h1 id="site-name">
              <a href="<?php echo $base_path; ?>" title="<?php echo t('Home'); ?>" rel="home">
            	  <span><?php echo $site_name; ?></span>
              </a>
            </h1>
          <?php endif; ?>
          
          <?php if ($site_slogan): ?>
            <div id='site-slogan'><?php print $site_slogan; ?></div>
          <?php endif; ?>

        </div>
        
        <?php if ($search_box): ?>
          <div class="span-9 prefix-1 last">
            <?php echo $search_box; ?>
  				</div>
  		  <?php endif; ?>
      

        <hr/>

		  <?php endif; ?>		
		    	      
      <?php if ($header): ?>
        <div id="header-region" class="span-24">
          <?php echo $header; ?>
        </div>

        <hr/>
      <?php endif; ?>
				
			<!-- ______________________ Navigation _______________________ -->

      <?php if (!empty($primary_links) or !empty($secondary_links)): ?>
        <div id="navigation" class="span-23 prefix-1 menu <?php if (!empty($primary_links)) { echo "withprimary"; } if (!empty($secondary_links)) { echo " withsecondary"; } ?> ">

          <?php if (!empty($primary_links)): ?>
            <div id="primary">
              <?php echo theme('links', $primary_links, array('class' => 'links primary-links')); ?>
            </div>
          <?php endif; ?>
          
          <?php if (!empty($secondary_links)): ?>
            <hr>
            <div id="secondary">
              <?php echo theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
            </div>
          <?php endif; ?>

        </div> <!-- /navigation -->

        <hr/>

      <?php endif; ?>

      <!-- ______________________ MAIN _______________________ -->

      <div id="main" class="span-24">

        <?php if ($left): ?> <!-- SIDEBAR LEFT -->
          <div id="sidebar-left" class="span-4 column sidebar colborder">
            <div id="sidebar-left-inner" class="inner">
              <?php echo $left ?>
            </div>
          </div>
        <?php endif; ?> <!-- /sidebar-left -->

        <?php if ($left && $right): ?>
          <div id="content" class="span-14 colborder">
        <?php elseif (!$left && $right): ?>
          <div id="content" class="span-19 colborder">
        <?php elseif ($left && !$right): ?>
          <div id="content" class="span-19 last">
        <?php elseif (!$left && !$right): ?>
          <div id="content" class="span-24">
        <?php endif; ?>

            <?php if ($breadcrumb || $title || $tabs || $help || $messages || $mission): ?>
            
              <div id="content-header">
              
                <?php echo $breadcrumb; ?>
                
                <?php if ($title): ?>
                  <h1 class="title"><?php echo $title; ?></h1>
                <?php endif; ?>
                
                <?php if ($mission): ?>
                  <div id="mission"><?php echo $mission; ?></div>
                <?php endif; ?>
                
                <?php echo $messages; ?>
                
                <?php if ($tabs): ?>
                  <div class="tabs"><?php echo $tabs; ?></div>
                <?php endif; ?>
                
                <?php echo $help; ?>
              
              </div> <!-- /#content-header -->
            
            <?php endif; ?>
            
            <div id="content-area"> <!-- CONTENT AREA -->
              <?php echo $content; ?>
            </div>
            
            <?php echo $feed_icons; ?>

          </div> <!-- /content -->

          <?php if ($right): ?> <!-- SIDEBAR RIGHT -->
            <div id="sidebar-right" class="span-4 last column sidebar">
              <div id="sidebar-right-inner" class="inner">
                <?php echo $right; ?>
              </div>
            </div> <!-- /sidebar-right -->
          <?php endif; ?>

				<hr/>

      </div> <!-- /main -->
	    
      <!-- ______________________ FOOTER _______________________ -->
      		
      <?php if(!empty($footer)): ?>
        <div id="footer" class="span-24">
          <?php echo $footer; ?>
        </div> <!-- /footer -->
      <?php endif; ?>

      <?php echo $closure; ?>

    </div> <!-- / .container -->
	
	  <?php 
      global $user;
      if ($user->uid == 1) { 
        echo '<a id="switchgrid" class="off" href="#" title="turn the grid on or off">Show/Hide Grid</a>' ;
      }
    ?>
	
	</body>
</html>